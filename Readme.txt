LEDs Animations with interrupts 

Author.
Jeronimo Jaramillo Bejarano
email: jeronimojaramillo.99@gmail.com

Objective. 

Design a temperature control system with a command parser using the ring buffer with the UART protocol, 
the command parser has the next structure:

/// Command messages ///

"*READ_TEMPERATURE#"  -> Send the last sample of the temperature to the host.
"*READ_FAN_SPEED#"    -> Calculate the percentage of the fan base of the duty cycle and sends it to the host.
"*SET_FAN_SPEED#"     -> Change the duty cycle of the PWM (DMA) to differents values: 0%, 25%, 50%, 75% and 100%.
"*CLOSE_DOOR#"        -> Set the state of the door to closed.
"*OPEN_DOOR#"         -> Set the state of the door to Open.
"*GET_DOOR_POSITION#" -> Sends the state of the door to the host:  0 is Opened and 1 is Closed.
"*GET_FW_VERSION#"    -> Sends the FW version to the host.
"*GET_UNIT_TICK#"     -> sends the total time the unit has been running.
"*GET_HEATER_STATE#"  -> Send the state of the heater, indicating ig ints turned ON or OFF: 0 is OFF and 1 is ON.
"*ACK#"               -> Send the confirmation correct command.	
"*NACK#"              -> Send the invalid confirmation command.

PLATFORM.
The board STM32F429ZI discovery 1 will be used on this development. 

FUNCTIONALITY.

* If "*READ_TEMPERATURE#" is sent, the system return to the host the last value of the sample temperature of the bmp280 sensor.
* If "*READ_FAN_SPEED#" is sent, the system returns the value of the actual value of PWM from the fan.
* If "*SET_FAN_SPEED#" is sent, the system does a linear increment of the PWM(DMA) every time it is sent.
* If "*CLOSE_DOOR#" is sent, the system put the GPIO on a high level and sends the "*ACK#"  message.
* If "*OPEN_DOOR#" is sent, the system put the GPIO on a low level and sends the "*ACK#"  message.
* If "*GET_DOOR_POSITION#" is sent, the systems return the state of the Door. 
* If "*GET_FW_VERSION#" is sent, the system returns the FW version most recently.  
* If "*GET_UNIT_TICK# is sent, the system returns the date format of time ON of the system
* If "*GET_HEATER_STATE#" is sent, the system return the state of the Heater.



  